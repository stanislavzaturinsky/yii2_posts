<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title:ntext',
            [
                'label'     => 'Text',
                'attribute' => 'postDescription',
                'value'     => $model->postDescription->text
            ],
            [
                'label'     => 'Language code',
                'attribute' => 'postDescription',
                'value'     => $model->postDescription->language_code
            ],
            'status',
            [
                'label'     => 'Created by',
                'attribute' => 'userCreated',
                'value'     => function($model) {
                    if($model->created_by) {
                        return $model->userCreated->last_name . ' ' . $model->userCreated->first_name;
                    } else {
                        return 'Guest';
                    }
                }
            ],
            [
                'label'     => 'Updated by',
                'attribute' => 'userUpdated',
                'value'     => function($model) {
                    if($model->updated_by) {
                        return $model->userUpdated->last_name . ' ' . $model->userUpdated->first_name;
                    } else {
                        return 'Guest';
                    }
                }
            ],
            [
                'label'     => 'Created at',
                'attribute' => 'created_at',
                'value'     => date("F j, Y, g:i a", $model->created_at)
            ],
            [
                'label'     => 'Updated at',
                'attribute' => 'updated_at',
                'value'     => date("F j, Y, g:i a", $model->updated_at)
            ],
        ],
    ]) ?>

</div>
<style>
    table.detail-view th {
        width: 15%;
    }

    table.detail-view td {
        width: 85%;
    }
</style>