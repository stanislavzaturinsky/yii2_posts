<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelPost app\models\Post */
/* @var $modelPostDescription app\models\PostDescription */

$this->title = 'Update Post: ' . $modelPost->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelPost->title, 'url' => ['view', 'id' => $modelPost->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelPost'            => $modelPost,
        'modelPostDescription' => $modelPostDescription,
    ]) ?>

</div>
