<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title:ntext',
            [
                'label'     => 'Created by',
                'attribute' => 'userCreated',
                'value'     => function($model) {
                    if($model->created_by) {
                        return $model->userCreated->last_name . ' ' . $model->userCreated->first_name;
                    } else {
                        return 'Guest';
                    }
                }
            ],
            [
                'label'     => 'Updated by',
                'attribute' => 'userUpdated',
                'value'     => function($model) {
                    if($model->updated_by) {
                        return $model->userUpdated->last_name . ' ' . $model->userUpdated->first_name;
                    } else {
                        return 'Guest';
                    }
                }
            ],
            [
                'label'     => 'Created at',
                'attribute' => 'created_at',
                'value'     => function($model) {
                    return date("F j, Y, g:i a", $model->created_at);
                }
            ],
            [
                'label'     => 'Updated at',
                'attribute' => 'updated_at',
                'value'     => function($model) {
                    return date("F j, Y, g:i a", $model->updated_at);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
