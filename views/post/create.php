<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $modelPost app\models\Post */
/* @var $modelPostDescription app\models\PostDescription */

$this->title = 'Create Post';
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelPost'            => $modelPost,
        'modelPostDescription' => $modelPostDescription,
    ]) ?>

</div>
