<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Post;
use app\models\PostDescription;
use yii\captcha\Captcha;


/* @var $this yii\web\View */
/* @var $modelPost app\models\Post */
/* @var $modelPostDescription app\models\PostDescription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelPost, 'title')->textInput() ?>

    <?= $form->field($modelPostDescription, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($modelPostDescription, 'language_code')->dropDownList(PostDescription::getLanguagesCode()) ?>

    <?= $form->field($modelPost, 'status')->dropDownList([
            Post::STATUS_ACTIVE   => 'active',
            Post::STATUS_INACTIVE => 'inactive',
    ]) ?>

    <?php if(Yii::$app->user->isGuest) {
        echo $form->field($modelPost, 'captcha')->widget(Captcha::className(),
            ['template' => '<div class="captcha_img">{image}</div>'
                . '<a class="refreshcaptcha" href="#"></a>'
                . 'Verification Code{input}',
            ])->label(FALSE);
    } ?>

    <div class="form-group">
        <?= Html::submitButton($modelPost->isNewRecord ? 'Create' : 'Update', ['class' => $modelPost->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
