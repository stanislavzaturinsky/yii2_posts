<?php
$_path = realpath(__DIR__."/../db")."/data.sqlite3";

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:' . $_path,
];
