<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $captcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['title', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'];
        $scenarios[self::SCENARIO_UPDATE] = ['title', 'status', 'updated_by', 'updated_at'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'message' => 'Title is required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            [
                ['title'], 'string',
                'min' => 10, 'tooShort' => 'The title is less then 10 symbols',
                'max' => 100, 'tooLong' => 'The title is more than 100 symbols',
                'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]
            ],
            [
                'status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE],
                'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]
            ],
            [
                ['created_by', 'updated_by'], 'default', 'value' => 0,
                'when' => function($model) {
                    return Yii::$app->user->isGuest;
                },
                'on' => [self::SCENARIO_CREATE]
            ],
            [
                ['created_by', 'updated_by'], 'default', 'value' => Yii::$app->user->id,
                'when' => function($model) {
                    return !Yii::$app->user->isGuest;
                },
                'on' => [self::SCENARIO_CREATE]
            ],
            ['created_at', 'default', 'value' => time(), 'on' => [self::SCENARIO_CREATE]],
            ['updated_at', 'default', 'value' => time(), 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            [
                'captcha', 'captcha', 'when' => function($model) {
                    return Yii::$app->user->isGuest;
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostDescription() {
        return $this->hasOne(PostDescription::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdated() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'updated_by']);
    }
}
