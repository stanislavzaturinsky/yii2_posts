<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_description".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language_code
 * @property string $text
 */
class PostDescription extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['post_id', 'language_code', 'text'];
        $scenarios[self::SCENARIO_UPDATE] = ['post_id', 'language_code', 'text'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required', 'message' => 'Title is required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            [
                ['text'], 'string',
                'min' => 10, 'tooShort' => 'The text is less then 10 symbols',
                'max' => 2000, 'tooLong' => 'The text is more than 100 symbols',
                'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]
            ],
            [
                'language_code', 'in', 'range' => array_keys(static::getLanguagesCode()),
                'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'language_code' => 'Language Code',
            'text' => 'Text',
        ];
    }

    public static function getLanguagesCode() {
        return [
            'en' => 'english',
            'ru' => 'russian',
            'pl' => 'polish',
        ];
    }
}
