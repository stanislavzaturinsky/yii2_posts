<?php

namespace app\controllers;

use app\models\PostDescription;
use Yii;
use app\models\Post;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\filters\AccessControl;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'create', 'view', 'update', 'delete'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['login'],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['logout'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = POST::findOne($id);
        if($model) {
            return $this->render('view', [
                'model' => $model,
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Post not exist");
            return $this->redirect(['/post/index']);
        }
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelPost            = new Post();
        $modelPostDescription = new PostDescription();

        if(Yii::$app->request->isPost) {
            $tr = Yii::$app->db->beginTransaction();
            $modelPost->scenario            = Post::SCENARIO_CREATE;
            $modelPostDescription->scenario = PostDescription::SCENARIO_CREATE;
        }

        if ($modelPost->load(Yii::$app->request->post()) && $modelPostDescription->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$modelPost, $modelPostDescription])) {

            if ($modelPost->save(false)) {

                $post_id = Yii::$app->db->getLastInsertID();
                if ($post_id) {

                    $modelPostDescription->post_id = $post_id;
                    if ($modelPostDescription->save(false)) {
                        $tr->commit();
                        Yii::$app->session->setFlash('success', "Post created!");
                        return $this->redirect(['view', 'id' => $modelPost->id]);
                    }
                }
            }

            $tr->rollBack();
            Yii::$app->session->setFlash('error', "Post not created");
            return $this->redirect(['/post/create']);
        } else {
            return $this->render('create', [
                'modelPost'            => $modelPost,
                'modelPostDescription' => $modelPostDescription,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $modelPost            = POST::findOne($id);
        $modelPostDescription = PostDescription::findOne(['post_id' => $id]);

        if(!$modelPost) {
            Yii::$app->session->setFlash('error', "Post not exist");
            return $this->redirect(['/post/index']);
        }

        if(Yii::$app->request->isPost) {
            $tr = Yii::$app->db->beginTransaction();
            $modelPost->scenario            = Post::SCENARIO_UPDATE;
            $modelPostDescription->scenario = PostDescription::SCENARIO_UPDATE;
        }

        if ($modelPost->load(Yii::$app->request->post()) && $modelPostDescription->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$modelPost, $modelPostDescription])) {

            $modelPost->updated_at = time();
            if(!Yii::$app->user->isGuest) {
                $modelPost->updated_by = Yii::$app->user->id;
            } else {
                $modelPost->updated_by = 0;
            }

            if ($modelPost->save(false) && $modelPostDescription->save(false)) {
                $tr->commit();
                Yii::$app->session->setFlash('success', "Post updated!");
                return $this->redirect(['view', 'id' => $modelPost->id]);
            }

            $tr->rollBack();
            Yii::$app->session->setFlash('error', "Post not updated");
            return $this->redirect(['update', 'id' => $modelPost->id]);
        } else {
            return $this->render('update', [
                'modelPost'            => $modelPost,
                'modelPostDescription' => $modelPostDescription,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $post            = POST::findOne($id);
        $postDescription = PostDescription::findOne(['post_id' => $id]);

        if ($post && $postDescription && $post->delete() && $postDescription->delete()) {
            Yii::$app->session->setFlash('success', "Post delete successful");
        } else {
            Yii::$app->session->setFlash('error', "Error on deleting post");
        }
        return $this->redirect(['/post/index']);
    }
}
